const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const WebpackGitHash = require('webpack-git-hash')
const { resolve } = require('path')

module.exports = () => ({

  entry: {

    main: [
      'react-hot-loader/patch',
      // activate HMR for React

      'webpack-dev-server/client?http://localhost:3000',
      // bundle the client for webpack-dev-server
      // and connect to the provided endpoint

      'webpack/hot/only-dev-server',
      // bundle the client for hot reloading
      // only- means to only hot reload for successful updates

      './index.js',
      // the entry point of our app
    ],

    vendor: [
      'babel-polyfill',
      'react'
    ]
  },

  output: {
    // filename: '[chunkhash].[name].js',
    filename: '[name].[githash].js',
    // the output bundle

    path: resolve(__dirname, './dist'),

    publicPath: '/'
    // necessary for HMR to know where to load the hot update chunks
  },

  context: resolve(__dirname, './src'),

  devtool: 'inline-source-map',

  devServer: {
    host: 'localhost',
    port: 3000,

    hot: true,
    // enable HMR on the server

    contentBase: resolve(__dirname, './dist'),
    // match the output path

    publicPath: '/',
    // match the output `publicPath`
  },

  resolve: {
    alias: {
      components: resolve(__dirname, './src/components'),
      containers: resolve(__dirname, './src/containers'),
      styles: resolve(__dirname, './src/styles'),
      models: resolve(__dirname, './src/models'),
      services: resolve(__dirname, './src/services')
    }
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          'babel-loader'
        ],
        exclude: {
          test: /node_modules/
        }
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ],
        exclude: {
          test: /node_modules/
        }
      },
      {
        test: /\.html$/,
        use: [
          'html-loader'
        ],
        exclude: {
          test: /node_modules/
        }
      },
      {
        test: /\.svg?$/,
        use: [
          'file-loader?name=icons/[name].[ext]&publicPath=./'
        ]
      }
    ]
  },

  plugins: [
    new CopyWebpackPlugin([
      { from: './images/*' },
      { from: './fonts/**/*' }
    ]),

    // Reference: https://github.com/ampedandwired/html-webpack-plugin
    // Render index.html
    new HtmlWebpackPlugin({
      template: 'index.html'
    }),

    new webpack.HotModuleReplacementPlugin(),
    // enable HMR globally

    new webpack.NamedModulesPlugin(),
    // prints more readable module names in the browser console on HMR updates

    new webpack.NoEmitOnErrorsPlugin(),

    new WebpackGitHash(),
    // add git hash to output filename by using [githash] in webpack output settings

    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor', 'manifest'] // Specify the common bundle's name.
    })
  ]
})

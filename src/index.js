import React from 'react'
import { render } from 'react-dom'
import { Router, hashHistory } from 'react-router'
import { Provider } from 'react-redux'
import { syncHistoryWithStore } from 'react-router-redux'

import { AppContainer as AppContainerHMR } from 'react-hot-loader'

import DevTools from 'components/DevTools'

import configureStore from './store/configureStore'
import routes from './routes'
import './styles/main.scss'

const store = configureStore(hashHistory)
const history = syncHistoryWithStore(hashHistory, store)

const bootApp = () => {
  render(
    <AppContainerHMR>
      <Provider store={store}>
        <div>
          <Router history={history} routes={routes} />
          <DevTools />
        </div>
      </Provider>
    </AppContainerHMR>,
    document.querySelector('#root')
  )
}

bootApp()

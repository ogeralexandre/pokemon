import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import { AppContainerReducer } from 'containers/AppContainer'
import { HomeContainerReducer } from 'containers/HomeContainer'
import { DetailContainerReducer } from 'containers/DetailContainer'

export default function createReducer(asyncReducers) {
  return combineReducers({
    routing: routerReducer,
    appContainer: AppContainerReducer,
    homeContainer: HomeContainerReducer,
    detailContainer: DetailContainerReducer,
    ...asyncReducers
  })
}

import { get } from './http'

const URL = 'http://pokeapi.co/api/v2'

export function getPokemons() {
  return get(`${URL}/pokemon/?limit=9`)
}

export function getPokemon(id) {
  return get(`${URL}/pokemon/${id}`)
}

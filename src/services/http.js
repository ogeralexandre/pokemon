import 'isomorphic-fetch'

function getDefaultHeaders() {
  return {
    'Content-Type': 'application/json'
  }
}

function handleError(res) {
  if (!res.ok) {
    console.error('An error occurred') // eslint-disable-line no-console
    return Promise.reject(res.json().catch(err => ({ err })))
  }
  return res.json()
    .catch(err => ({ err })) // handle valid res with no json response (cause crash)
}

function fetcher(url, options) {
  return fetch(url, options)
    .then(handleError)
}

function get(url, headers = {}) {
  return fetcher(url, {
    headers: Object.assign({}, getDefaultHeaders(), headers)
  })
}

function post(url, body, headers = {}) {
  return fetcher(url, {
    method: 'POST',
    headers: Object.assign({}, getDefaultHeaders(), headers),
    body: JSON.stringify(body)
  })
}

// `post` version without json body
function postData(url, body, headers = {}) {
  return fetcher(url, {
    method: 'POST',
    headers: Object.assign({}, getAuthorizationHeaders(), headers),
    body
  })
}

function put(url, body, headers = {}) {
  return fetcher(url, {
    method: 'PUT',
    headers: Object.assign({}, getDefaultHeaders(), headers),
    body: JSON.stringify(body)
  })
}

function remove(url, headers = {}) {
  return fetcher(url, {
    method: 'DELETE',
    headers: Object.assign({}, getDefaultHeaders(), headers)
  })
}

export { get, post, postData, put, remove }

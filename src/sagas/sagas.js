import { fork } from 'redux-saga/effects'

import {
  watchAskForLoremIpsum
} from 'containers/AppContainer'
import {
  watchSearchPokemon,
  watchGetPokemons
} from 'containers/HomeContainer'
import {
  watchGetPokemon
} from 'containers/DetailContainer'

export default function* root() {
  yield [
    fork(watchAskForLoremIpsum),
    fork(watchSearchPokemon),
    fork(watchGetPokemons),
    fork(watchGetPokemon)
  ]
}

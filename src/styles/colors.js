export const color01 = '#282741' // dark grey
export const color02 = '#393757' // medium grey
export const color03 = '#645F86' // light grey
export const color04 = '#FF5781' // red/pink
export const color04Darken = '#D4486B' // red/pink darken
export const color05 = '#F1F2F6' // white

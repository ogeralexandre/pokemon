export const mainFont = {
  fontFamily: "Cabin",
  fontStyle: "normal",
  fontWeight: "normal",
  src: "url('fonts/Cabin/Cabin-Regular.ttf') format('truetype')"
}

export const MainFontMedium = {
  fontFamily: "Cabin Medium",
  fontStyle: "normal",
  fontWeight: "normal",
  src: "url('fonts/Cabin/Cabin-Medium.ttf') format('truetype')"
}

import React from 'react'
import { Route, IndexRedirect, Redirect } from 'react-router'

import AppContainer from 'containers/AppContainer'
import HomeContainer from 'containers/HomeContainer'
import DetailContainer from 'containers/DetailContainer'

export default (
  <Route path="/">
    <IndexRedirect to="home" />
    <Route path="/" component={AppContainer}>
      <Route path="home" component={HomeContainer} />
      <Route path="detail/:id" component={DetailContainer} />
    </Route>
    <Redirect from="*" to="/" />
  </Route>
)

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Grid } from 'react-bootstrap'
import { css, StyleSheet } from 'aphrodite'
import { color01, color04 } from 'styles/colors'
import { mainFont } from 'styles/fonts'

class AppContainer extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  }

  static defaultProps = {}

  componentWillMount() {}

  render() {
    const { children } = this.props
    return (
      <div className={css(styles.background)}>
        <Grid fluid={true} className={css(styles.container)}>
          {this.props.children}
        </Grid>
      </div>
    )
  }
}

const styles = StyleSheet.create({
  background: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    overflow: 'scroll',
    backgroundColor: color01,
    ':before': {
      content: "''",
      position: 'absolute',
      zIndex: 1,
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      opacity: 0.2,
      overflow: scroll,
      backgroundImage: `radial-gradient(circle farthest-side at center top, ${color04} 0%, ${color01} 400px)`
    }
  },
  container: {
    position: 'relative',
    zIndex: 2,
    maxWidth: '375px',
    fontFamily: [mainFont]
  }
})

const mapStateToProps = state => ({})

const mapDispatchToProps = {}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppContainer)

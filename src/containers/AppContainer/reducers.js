// @flow

import {
  LOREM_IPSUM
} from './action-types'

export const AppContainerState = {}

export default function reducer(state = AppContainerState, { type, payload }) {
  switch (type) {
    case LOREM_IPSUM: {
      return state
    }

    default:
      return state
  }
}

// @flow

import {
  LOREM_IPSUM
} from './action-types'

export function sayLoremIpsum() {
  return {
    type: LOREM_IPSUM,
    payload: 'lorem ipsum'
  }
}

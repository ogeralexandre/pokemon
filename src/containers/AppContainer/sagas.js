import { put, takeEvery, select } from 'redux-saga/effects'
import {
  LOREM_IPSUM
} from './action-types'
import {
  sayLoremIpsum
} from './actions'

function* AskForLoremIpsum(action) {
  try {
    const { payload } = action
    yield put(sayLoremIpsum(payload))
  } catch (e) {
    console.error(e)
  }
}

function* watchAskForLoremIpsum() {
  yield takeEvery([
    LOREM_IPSUM
  ], AskForLoremIpsum)
}

export { watchAskForLoremIpsum }

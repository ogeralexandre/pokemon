import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import Home from 'components/Home'
import { searchPokemon, getPokemons } from './actions'
import {
  getPokemons as getPokemonsSelector,
  getSearchValue
 } from './selectors'

class HomeContainer extends Component {
  static propTypes = {
    searchPokemon: PropTypes.func.isRequired,
    getPokemons: PropTypes.func.isRequired
  }

  static defaultProps = {}

  componentWillMount() {
    this.props.getPokemons()
  }

  render() {
    console.log(this.props)
    return (
      <Home {...this.props} />
    )
  }
}

const mapStateToProps = state => ({
  pokemons: getPokemonsSelector(state),
  searchValue: getSearchValue(state)
})

const mapDispatchToProps = {
  searchPokemon,
  getPokemons,
  push
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeContainer)

// @flow

import {
  GET_POKEMONS,
  GET_POKEMONS_SUCCESS,
  SEARCH_POKEMON,
  SEARCH_POKEMON_SUCCESS
} from './action-types'

export function getPokemons() {
  return {
    type: GET_POKEMONS
  }
}

export function getPokemonsSucceeded(pokemons: Array) {
  return {
    type: GET_POKEMONS_SUCCESS,
    payload: pokemons
  }
}


export function searchPokemon(query: String) {
  return {
    type: SEARCH_POKEMON,
    payload: query
  }
}

export function searchPokemonSucceeded(pokemons: Array) {
  return {
    type: SEARCH_POKEMON_SUCCESS,
    payload: pokemons
  }
}

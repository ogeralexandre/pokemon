export const getPokemons = state => state.homeContainer.get('pokemons')
export const getSearchValue = state => state.homeContainer.get('searchValue')
export const getPokemonsList = state => state.homeContainer.get('pokemonsList')

// @flow

import { List, Map } from 'immutable'

import {
  GET_POKEMONS_SUCCESS,
  GET_POKEMONS_FAILURE,
  SEARCH_POKEMON,
  SEARCH_POKEMON_SUCCESS,
  SEARCH_POKEMON_FAILURE
} from './action-types'

export const HomeContainerState = Map({
  pokemons: List(),
  pokemonsList: List(),
  searchValue: ''
})

export default function reducer(state = HomeContainerState, { type, payload }) {
  switch (type) {

    case SEARCH_POKEMON: {
      return state.merge({
        searchValue: payload
      })
    }

    case SEARCH_POKEMON_SUCCESS: {
      return state.merge({
        pokemons: payload
      })
    }

    case GET_POKEMONS_SUCCESS: {
      return state.merge({
        pokemonsList: payload
      })
    }

    case SEARCH_POKEMON_FAILURE: {
      return state
    }

    default:
      return state
  }
}

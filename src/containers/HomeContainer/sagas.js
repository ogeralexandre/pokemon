import R from 'ramda'
import { put, takeEvery, select } from 'redux-saga/effects'
import {
  GET_POKEMONS,
  SEARCH_POKEMON
} from './action-types'
import {
  searchPokemonSucceeded,
  getPokemonsSucceeded
} from './actions'
import {
  getPokemons as getPokemonsCall
} from 'services/pokeapi'
import { getPokemonsList } from './selectors'

function* searchPokemon(action) {
  try {
    const { payload } = action
    let result = []

    if ( payload.length !== 0 ) {
      const pokemons = yield select(getPokemonsList)
      const getPokemons = R.filter(R.compose(
        R.contains(payload),
        R.prop('name')
      ))
      result = getPokemons(pokemons.toJS())
    }
    
    yield put(searchPokemonSucceeded(result))
  } catch (e) {
    console.error(e)
  }
}

function* getPokemons() {
  try {
    const getPokemons = R.composeP(
      R.map(R.dissoc('url')),
      R.prop('results'),
      getPokemonsCall
    )
    const pokemons = yield getPokemons()
    yield put(getPokemonsSucceeded(pokemons))
  } catch (e) {
    console.error(e)
  }
}

function* watchSearchPokemon() {
  yield takeEvery([
    SEARCH_POKEMON
  ], searchPokemon)
}

function* watchGetPokemons() {
  yield takeEvery([
    GET_POKEMONS
  ], getPokemons)
}

export { watchSearchPokemon, watchGetPokemons }

// @flow

import { Map, List } from 'immutable'

import {
  GET_POKEMON_SUCCESS,
  GET_POKEMON_FAILURE
} from './action-types'

// 170	90	45	90	45	60
export const DetailContainerState = Map({
  pokemon: Map({
    name: '',
    types: List(),
    height: 0,
    weight: 0,
    stats: List()
  })
})

export default function reducer(state = DetailContainerState, { type, payload }) {
  switch (type) {

    case GET_POKEMON_SUCCESS: {
      return state.merge({
        pokemon: payload
      })
    }

    default:
      return state
  }
}

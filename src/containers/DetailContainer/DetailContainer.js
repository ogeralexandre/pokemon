import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import Detail from 'components/Detail'
import { getPokemon as getPokemonsSelector } from './selectors'
import { getPokemon } from './actions'

class DetailContainer extends Component {
  static propTypes = {}

  static defaultProps = {}

  componentWillMount() {
    this.props.getPokemon(this.props.name)
  }

  render() {
    return (
      <Detail {...this.props} />
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  name: ownProps.params.id,
  pokemon: getPokemonsSelector(state)
})

const mapDispatchToProps = {
  getPokemon,
  push
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailContainer)

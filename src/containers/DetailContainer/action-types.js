const BASE = 'zengularity/DetailContainer'

export const GET_POKEMON = `${BASE}/GET_POKEMON`
export const GET_POKEMON_SUCCESS = `${BASE}/GET_POKEMON_SUCCESS`
export const GET_POKEMON_FAILURE = `${BASE}/GET_POKEMON_FAILURE`

export { default } from './DetailContainer'
export { default as DetailContainerReducer } from './reducers'
export { watchGetPokemon } from './sagas'

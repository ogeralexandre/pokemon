import R from 'ramda'
import { put, takeEvery, select } from 'redux-saga/effects'
import {
  GET_POKEMON
} from './action-types'
import {
  getPokemonSucceeded
} from './actions'
import {
  getPokemon as getPokemonCall
} from 'services/pokeapi'

function* getPokemon(action) {
  try {
    const { payload } = action
    const getPokemon = R.composeP(
      R.evolve({
        stats: R.map(stat => ({
          score: R.prop('base_stat', stat),
          type: R.path(['stat', 'name'], stat)
        })),
        types: R.map(R.path(['type', 'name']))
      }),
      R.pick(['id', 'name', 'weight', 'height', 'stats', 'types']),
      R.partial(getPokemonCall, [payload])
    )

    const pokemon = yield getPokemon()
    yield put(getPokemonSucceeded(pokemon))
  } catch (e) {
    console.error(e)
  }
}

function* watchGetPokemon() {
  yield takeEvery([
    GET_POKEMON
  ], getPokemon)
}

export { watchGetPokemon }

// @flow

import {
  GET_POKEMON,
  GET_POKEMON_SUCCESS
} from './action-types'

export function getPokemon(name) {
  return {
    type: GET_POKEMON,
    payload: name
  }
}

export function getPokemonSucceeded(pokemon: Object) {
  return {
    type: GET_POKEMON_SUCCESS,
    payload: pokemon
  }
}

import React from 'react'
import PropTypes from 'prop-types'
import { css, StyleSheet } from 'aphrodite'
import { color03, color04, color05 } from 'styles/colors'
import { MainFontMedium } from 'styles/fonts'
import iconArrow from './images/arrow.svg'

const Item = ({ title, subtitle, action, icon }) => (
  <article
    className={css(styles.container)}
    onClick={action}
  >
    <div className={css(styles.circle)}>
      <span className={css(styles.circleInner)}>{title[0]}</span>
    </div>
    <h2 className={css(styles.title)}>{title}</h2>
    { subtitle &&
      <small className={css(styles.subtitle)}>
        {subtitle}
      </small>
    }
    <img
      className={css(styles.arrow)}
      src={icon}
      alt=""
    />
  </article>
)

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    height: '72px',
    color: color05,
    borderBottom: '2px solid rgba(100, 95, 134, 0.23)',
    cursor: 'pointer'
  },
  circle: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    margin: 'auto 0',
    width: '42px',
    height: '42px',
    border: `2px solid ${color04}`,
    borderRadius: '100%'
    // backgroundImage: 'url(https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/1.png)',
    // backgroundSize: 'cover',
    // backgroundRepeat: 'no-repeat'
  },
  circleInner: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    margin: 'auto 0',
    height: '20px',
    textAlign: 'center',
    lineHeight: '20px',
    fontFamily: [MainFontMedium],
    textTransform: 'uppercase',
    fontSize: '20px'
  },
  title: {
    position: 'absolute',
    top: 0,
    right: '60px',
    left: '60px',
    bottom: 0,
    margin: 'auto 0',
    height: '20px',
    fontSize: '16px',
    letterSpacing: '1px'
  },
  subtitle: {
    position: 'absolute',
    top: '32px',
    right: '60px',
    left: '60px',
    bottom: 0,
    margin: 'auto 0',
    height: '20px',
    letterSpacing: '1px',
    color: color03
  },
  arrow: {
    position: 'absolute',
    top: 0,
    right: '12px',
    bottom: 0,
    margin: 'auto 0',
    width: '20px',
    height: '20px'
  }
})

Item.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  icon: PropTypes.string,
  action: PropTypes.func.isRequired
}

Item.defaultProps = {
  icon: iconArrow,
  subtitle: null
}

export default Item

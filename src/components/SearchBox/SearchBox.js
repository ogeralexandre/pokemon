import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { css, StyleSheet } from 'aphrodite'
import { color05, color03 } from 'styles/colors'
import searchIcon from './images/search.svg'

class SearchBox extends Component {
  constructor(props) {
    super(props)
    console.log('rerender')
    this.state = {
      value: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(event) {
    this.setState({value: event.target.value})
  }

  handleSubmit(event) {
    const query = this.state.value
    console.log(query)
    this.props.action(query)
    event.preventDefault()
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps)
    this.setState({value: nextProps.searchValue})
  }

  render() {
    return (
      <form
        className={css(styles.container)}
        onSubmit={this.handleSubmit}
      >
        <label className={css(styles.label)}>
          Search
        </label>
        <input
          className={css(styles.input)}
          type="search"
          value={this.state.value}
          onChange={this.handleChange}
          placeholder="A pokemon…"
        />
        <button
          className={css(styles.icon)}
          onClick={this.handleSubmit}
        >
          <img src={searchIcon} alt="" />
        </button>
      </form>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    marginBottom: '36px'
  },
  icon: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    bottom: 0,
    width: '45px',
    height: '45px',
    padding: '10px',
    border: 'transparent',
    backgroundColor: 'transparent'
  },
  label: {
    marginTop: '32px',
    marginBottom: '8px',
    color: color05,
    fontWeight: 200,
    textTransform: 'uppercase'
  },
  input: {
    display: 'block',
    width: '100%',
    height: '45px',
    backgroundColor: 'transparent',
    borderRadius: '3px',
    borderColor: color03,
    color: color05,
    textIndent: '8px',
    letterSpacing: '1px',
    borderStyle: 'solid',
    ':active, :focus': {
      outline: 'none'
    },
    '::-webkit-input-placeholder': {
      color: color03
    }
  }
})

SearchBox.propTypes = {
  children: PropTypes.node,
  action: PropTypes.func.isRequired,
  searchValue: PropTypes.string.isRequired
}

SearchBox.defaultProps = {
  children: null
}

export default SearchBox

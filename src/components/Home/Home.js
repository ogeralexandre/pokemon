import React from 'react'
import PropTypes from 'prop-types'
import { css, StyleSheet } from 'aphrodite'
import logo from './images/logo.svg'
import SearchBox from 'components/SearchBox'
import Item from 'components/Item'

const Home = props => {
  const {
    pokemons,
    searchPokemon,
    searchValue,
    push
  } = props
  const hasPokemon = !pokemons.isEmpty()
  const logoClassName = css(styles.logo, hasPokemon && styles.logoHidden)
  const Pokemons = pokemons.map((pokemon) => (
    <Item
      title={pokemon.get('name')}
      key={pokemon.get('name')}
      action={() => {
        push(`/detail/${pokemon.get('name')}`)
      }}
    />
  ))

  return (
    <main>
      <img
        className={logoClassName}
        src={logo}
        alt="Logo pokémon"
      />
      <SearchBox action={searchPokemon} searchValue={searchValue} />
      <section>
        {Pokemons}
      </section>
    </main>
  )
}

const styles = StyleSheet.create({
  logo: {
    display: 'block',
    width: '205px',
    height: 'auto',
    margin: '112px auto 82px auto'
  },
  logoHidden: {
    display: 'none'
  }
})

Home.propTypes = {
  children: PropTypes.node
}

Home.defaultProps = {
  children: null
}

export default Home

import React from 'react'
import PropTypes from 'prop-types'
import { css, StyleSheet } from 'aphrodite'
import { color03 } from 'styles/colors'
import { MainFontMedium } from 'styles/fonts'
import weightIcon from './images/weight.svg'
import heightIcon from './images/height.svg'

const BodyStats = props => {
  const { height, weight } = props

  return (
    <section className={css(styles.container)}>
      <article className={css(styles.box)}>
        <div className={css(styles.title)}>Height</div>
        <img className={css(styles.icon)} src={heightIcon} alt="" />
        <h3 className={css(styles.count)}>{height}</h3>
      </article>
      <hr className={css(styles.separator)} />
      <article className={css(styles.box)}>
        <div className={css(styles.title)}>Weight</div>
        <img className={css(styles.icon)} src={weightIcon} alt="" />
        <h3 className={css(styles.count)}>{weight}</h3>
      </article>
    </section>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: '40px'
  },
  separator: {
    display: 'inline-block',
    verticalAlign: 'top',
    height: '100px',
    width: '2px',
    margin: 0,
    backgroundColor: color03,
    opacity: '0.2',
    border: 0
  },
  box: {
    display: 'inline-block',
    width: '48%',
    paddingTop: '12px',
    textAlign: 'center'
  },
  title: {
    marginBottom: '8px',
    textTransform: 'uppercase',
    color: color03,
    fontFamily: [MainFontMedium],
    fontSize: '12px',
    letterSpacing: '1px'
  },
  icon: {
    width: '28px',
    height: '28px'
  },
  count: {
    margin: 0,
    marginTop: '6px'
  }
})

BodyStats.propTypes = {
  height: PropTypes.number.isRequired,
  weight: PropTypes.number.isRequired
}

BodyStats.defaultProps = {}

export default BodyStats

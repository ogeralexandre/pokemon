import React from 'react'
import R from 'ramda'
import { Map } from 'immutable'
import PropTypes from 'prop-types'
import { css, StyleSheet } from 'aphrodite'
import {
  Radar,
  RadarChart,
  PolarGrid,
  Legend,
  PolarAngleAxis,
  PolarRadiusAxis,
  ResponsiveContainer
} from 'recharts'
import BodyStats from 'components/BodyStats'
import closeIcon from './images/close.svg'
import Item from 'components/Item'
import { color02, color04, color05, color04Darken } from 'styles/colors'

const Detail = ({ pokemon, push }) => {
  const goToSearchResult = R.partial(push, ['/home'])
  const stats = pokemon.get('stats').toJS()
  const types = pokemon.get('types').toJS().join(' - ')
  const data = R.map(stat => ({
      subject: R.prop('type', stat),
      A: R.prop('score', stat)
  }), stats)

  return (
    <main className={css(styles.chart)}>
      <Item
        title={pokemon.get('name')}
        subtitle={types}
        action={goToSearchResult}
        icon={closeIcon}
      />
      <ResponsiveContainer aspect={0.90} width="100%">
        <RadarChart cx={'50%'} cy={'50%'} outerRadius={150} data={data}>
          <PolarGrid stroke={color02} />
          <PolarRadiusAxis angle={30} domain={[0, 150]}/>
          {/* <Radar
            name="average"
            dataKey="A"
            stroke="#8884d8"
            fill="#8884d8"
            fillOpacity={0.6}
          /> */}
          <Radar
            name={pokemon.get('name')}
            dataKey="A"
            stroke={color04}
            fill={color04Darken}
            fillOpacity={0.6}
          />
          <PolarAngleAxis
            textAnchor='middle'
            stroke={color05}
            axisLine={false}
            tickLine={false}
            orientation='inner'
            dataKey="subject"
          />
          <Legend />
        </RadarChart>
      </ResponsiveContainer>
      <div>
        <BodyStats
          height={pokemon.get('height')}
          weight={pokemon.get('weight')}
        />
      </div>
    </main>
  )
}

const styles = StyleSheet.create({
  chart: {
    color: color05
  }
})

Detail.propTypes = {
  children: PropTypes.node
}

Detail.defaultProps = {
  children: null
}

export default Detail

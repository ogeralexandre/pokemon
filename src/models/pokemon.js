// @flow

export type Pokemon = {
  name: string,
  type: Array<string>,
  height: number,
  weight: number,
  stats: {
    special_attack: number,
    special_defense: number,
    special_speed: number,
    attack: number,
    defense: number,
    hp: number
  }
}

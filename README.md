# Pokemon Zengularity homework

## Getting started

### Yarn

Before you start, you'll first need to install Yarn on your system.

Installation guidelines: https://yarnpkg.com/en/docs/install

### CLI

`yarn install` install dependencies

`yarn start` launch dev server on http://localhost:8080
